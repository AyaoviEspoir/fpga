module Delay_Reset(
  input Clk,			// Input clock signal
  input Button,		// Input reset signal (external Button).
  output reg Reset		// Output reset signal (delayed).
    );
	 //--------------------------------------------------------
  reg LocalReset;
  reg [22:0]Count;	// assumes count is null in FPGA configuration
  //------------------------------------------------------------------
  
  always @(posedge Clk) begin		// activates every clock edge
    LocalReset <= Button;			// localise the reset signal.
	 
	 if(LocalReset) begin			// reset block
	   Count <= 0;						// reset Count to 0
		Reset <= 1'b1;					// reset the output to 1
	 end else if(&Count) begin		// when Count is all 1s...
	   Reset <= 1'b0;					// release the output signal
		// Count remains unchanged	// and do nothing else.
	 end else begin					// otherwise
	   Reset <= 1'b1;					// make sure the output signal is high
	   Count <= Count + 1'b1;		// and increment Count
	 end
  end
  //--------------------------------------------------------------------------
endmodule
//--------------------------------------------------------------------------