module Clock(
	input Clk_100M,
	input resetBtn,
	input minuteBtn,
	input hourBtn,
	input [7:0]SlideButtons,
	
	output [3:0]SegmentDrivers,
	output [7:0]SevenSegment,
	output reg [5:0]LEDs
	);
reg [29:0]Count;
reg [26:0]SecondCounter;
reg [27:0]MinuteCounter;
reg [5:0]Seconds;
reg [3:0]MinuteUnits;
reg [3:0]MinuteTens;
reg [3:0]HourUnits;
reg [3:0]HourTens;
reg minuteBtnPrevState;
reg hourBtnPrevState;

always @(posedge Clk_100M) begin 
Count <= Count + 1'b1;
	// check for minute of hour button being pressed.
	if (minuteBtnPrevState==1'b0 && minuteBtnNextState==1) begin
		minuteBtnPrevState <= minuteBtnNextState;
		MinuteUnits <= MinuteUnits + 1'b1;
		if (MinuteUnits == 4'd9) begin
			MinuteTens <= MinuteTens + 1'b1;
			MinuteUnits <= 0;
			if (MinuteTens == 4'd5) begin
				MinuteTens <= 0;
			end
		end
	end 
	else begin
	minuteBtnPrevState <= minuteBtnNextState;
	end
	if (hourBtnPrevState==1'b0 && hourBtnNextState==1) begin
		hourBtnPrevState <= hourBtnNextState;
		HourUnits <= HourUnits + 1'b1;
		if (HourUnits == 4'd9) begin
			HourTens <= HourTens + 1'b1;
			HourUnits <= 0;
			
		end
		if (HourTens == 4'd2 && HourUnits == 4'd3) begin
			HourUnits <= 0;
			HourTens <= 0;
		end
	end 
		else begin
		hourBtnPrevState <= hourBtnNextState;
	end
			if (SecondCounter == 27'd99999999) begin
		//	if (SecondCounter == 27'd1666666) begin
//			if (SecondCounter == 27'd27777) begin
				Seconds <= Seconds + 1'b1;
				if (Seconds == 6'd59) begin
					MinuteUnits <= MinuteUnits + 1'b1;
					Seconds <= 0;				
					if (MinuteUnits == 4'd9) begin
						MinuteTens <= MinuteTens + 1'b1;
						MinuteUnits <= 0;
						
						if (MinuteTens == 4'd5) begin
							HourUnits <= HourUnits + 1'b1;
							MinuteTens <= 0;
							
							if (HourUnits == 4'd9) begin
								HourTens <= HourTens + 1'b1;
								HourUnits <= 0;
								
							end 
							if (HourTens == 4'd2 && HourUnits == 4'd3) begin
								Seconds <= 0;
								MinuteUnits <= 0;
								MinuteTens <= 0;
								HourUnits <= 0;
								HourTens <= 0;
							end
						end
					end
				end
				SecondCounter <= 0;
			end else begin
				SecondCounter <= SecondCounter + 1'b1;
		end
		if (Reset) begin
			Seconds <= 0;
			MinuteUnits <= 0;
			MinuteTens <= 0;
			HourUnits <= 0;
			HourTens <= 0;
		end
		
		// end of normal run
end	// end always.

always @(*) begin 
	LEDs <= Seconds;
end
	wire Reset;		// define a local wire to carry the Reset signal.

	Delay_Reset Delay_Reset1(Clk_100M, resetBtn, Reset);
	
	SS_Driver SS_Driver1(
		Clk_100M, Reset,
		HourTens, HourUnits, MinuteTens, MinuteUnits,
		SlideButtons, SegmentDrivers, SevenSegment
	);
	wire minuteBtnNextState;
	wire hourBtnNextState;
	
	Debounce DebounceMinute(Clk_100M, minuteBtn, minuteBtnNextState);
	Debounce DebounceHour(Clk_100M, hourBtn, hourBtnNextState);
endmodule
