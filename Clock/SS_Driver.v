module SS_Driver(
	input Clk, Reset,
	input [3:0]BCD3, BCD2, BCD1, BCD0, 	// binary-coded decimal input
	input [7:0]SlideButtons,
	output reg [3:0]SegmentDrivers,			// Digit driver (active low)
	output reg [7:0]SevenSegment			// Segments (active low)
	);
//----------------------------------------------------------------------------------


// Make use of a subcircuit to decode the BCD to seven-segment (SS)
wire [6:0]SS[3:0];

BCD_Decoder BCD_Decoder0(BCD0, SS[0]);
BCD_Decoder BCD_Decoder1(BCD1, SS[1]);
BCD_Decoder BCD_Decoder2(BCD2, SS[2]);
BCD_Decoder BCD_Decoder3(BCD3, SS[3]);
//----------------------------------------------------------------------------------

// Counter to reduce the 100 MHz clock to 762.939 Hz (100 MHz / 2^17)
reg [16:0]Count;
reg [8:0]DisplayCount;

// Scroll through the digits, switching one on at a time.
always @(posedge Clk) begin
	Count <= Count + 1'b1;
	DisplayCount <= DisplayCount + 1'b1;
	if (Reset) SegmentDrivers <= 4'hE;		// turn off the display.
	else if (&Count) SegmentDrivers <= {SegmentDrivers[2:0], SegmentDrivers[3]};
end
//----------------------------------------------------------------------------------


always @(*) begin				// This describes a purely combinatorial circuit.
	SevenSegment[7] <= 1'b1;	// decimal point always off
	if (Reset || DisplayCount > SlideButtons) begin
		SevenSegment[6:0] <= 7'h7F;	// all off during reset

	end else begin
		case(~SegmentDrivers)	// connect the correct signals depending on which digit is on at this point.
			4'h1   : SevenSegment[6:0] <= ~SS[0];
			4'h2   : SevenSegment[6:0] <= ~SS[1];
			4'h4   : SevenSegment[6:0] <= ~SS[2];
			4'h8   : SevenSegment[6:0] <= ~SS[3];
			default: SevenSegment[6:0] <= 7'h7F;
		endcase
	end
end

endmodule